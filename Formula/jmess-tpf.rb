class JmessTpf < Formula
  desc "JMess - A utility to save your audio connections (mess)"
  homepage "https://ccrma.stanford.edu/groups/soundwire/software/jmess/"
  version "1.0.1-tpf"
  url "https://gitlab.zhdk.ch/TPF/jmess/repository/archive.zip?ref=v1.0.1-tpf"
  sha256 "b5a7bab9bcc42da71ec7b0c55a04038df456a43d79d2f30ed83aca290e800439"

  bottle do
    root_url "https://gitlab.zhdk.ch/TPF/homebrew-tpf-bottles/raw/master/bottles"
    sha256 "a280e29c751c40313ab72aa953c0994312558c4d1a6c54acacac6f5ba3eac719" => :sierra
    sha256 "386a7b413100c65a17a855378319e3743f97837bfb77f9355692d79dc0bbaa10" => :el_capitan
    sha256 "e2d1b418b348e2b7b4e716832ee47f6ffa98124b221b037f7253774072dd554a" => :yosemite
  end

  depends_on "qt" => :build
  depends_on "jack2"

  def install
    `( cd jmess/src; PATH=$PATH:#{Formula["qt"].opt_prefix} ./build )`
    if $? != 0
      return false
    end
    bin.install "jmess/src/jmess"
  end

  test do
    system "#{bin}/jmess", "--help"
  end
end

