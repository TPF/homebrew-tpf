class Jacktrip < Formula
  desc "JackTrip is a Linux and Mac OS X-based system used for multi-machine audio network performance over the Internet."
  homepage "https://ccrma.stanford.edu/groups/soundwire/software/jacktrip/"
  url "https://gitlab.zhdk.ch/TPF/jacktrip/repository/archive.zip?ref=v1.1-tpf"
  version "1.1-tpf"
  sha256 "bc11d834706dec5d26f0dbe0d286501487f6b98adb6e47e6ff15a72073639aa7"

  bottle do
    root_url "https://gitlab.zhdk.ch/TPF/homebrew-tpf-bottles/raw/master/bottles"
    sha256 "16652ba65999149acfa32499eba2117d433cb70ec56a59922f362f843ab479c1" => :sierra
    sha256 "f4949bb4d5a02e7558a5796beab0f92a666c8c02360cf8f80df420f6de159048" => :el_capitan
    sha256 "77ab091b47424ae571a5568da4eb185d7dc671cf158e214363b3dccf4015eb23" => :yosemite
  end

  depends_on "qt" => :build
  depends_on "jack2"

  def install
    `( cd src; PATH=$PATH:#{Formula["qt"].opt_prefix} ./build )`
    if $? != 0
      return false
    end
    bin.install "src/jacktrip"
  end

  test do
    system "#{bin}/jacktrip", "--help"
  end
end

