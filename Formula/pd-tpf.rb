class PdTpf < Formula
  desc "realtime computer music and graphics system"
  homepage "https://puredata.info/"
  version "0.47-1"
  url "https://sourceforge.net/code-snapshots/git/p/pu/pure-data/pure-data.git/pure-data-pure-data-c17754d54d25edbbde0edacf0f887b1d80736562.zip"
  sha256 "9aef5f593b3020530c772a2c9e1e7f2cf5a1a5d6fe79012415f1e96e27c1fc72"

  bottle do
    root_url "https://gitlab.zhdk.ch/TPF/homebrew-tpf-bottles/raw/master/bottles"
    sha256 "12b5bd30f05351c33977bee37791054a049947a9f31933a803c5cf37d59378df" => :sierra
    sha256 "d19ff61cc0976b9b4f30802d3b9ea5a9f09f21c6158187204cffab6d719b1376" => :el_capitan
    sha256 "cafa13c2489d3da1c3c43aef4c3a589e6b50d5add1448b6c8e2cb3a6735cffc0" => :yosemite
  end

  depends_on "automake" => :build
  depends_on "autoconf" => :build
  depends_on "libtool" => :build
  depends_on "jack2"

  # jack2 from homebrew doesn't install Jackmp.framework, that's why
  # we want to compile pd against the common (non-weak) library
  patch :DATA

  def install
    system "./autogen.sh"
    system "./configure", "--enable-jack",
                          "--prefix=#{prefix}"
    system "make", "-j"
    system "make", "install"
  end

  test do
    system "false"
  end
end

__END__
diff --git a/src/Makefile.am b/src/Makefile.am
index 9c4e28e..d121341 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -139,7 +139,9 @@ pd_SOURCES += s_audio_jack.c
 
 if MACOSX
 # link to Jackmp on OSX
-pd_LDFLAGS += -weak_framework Jackmp
+#pd_LDFLAGS += -weak_framework Jackmp
+# for brew we want do actually use the lib
+pd_LDADD += -ljack
 else
 # link to Jack discovered by configure
 pd_LDADD += @JACK_LIBS@
