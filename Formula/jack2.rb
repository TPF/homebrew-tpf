class Jack2 < Formula
  desc "JACK low-latency audio server for multi-processor machines. It is a new implementation of the JACK server core features that aims in removing some limitations of the JACK1 design."
  homepage "http://jackaudio.org/"
  url "https://gitlab.zhdk.ch/TPF/jack2/repository/archive.zip?ref=v1.9.10-tpf"
  version "1.9.11"
  sha256 "cc0045e6e6681faee112e46ce1385ed85d8b00f87256a38b006c7b4b34109696"

  depends_on "pkg-config" => :build
  depends_on "python" => :build
  depends_on "aften"
  depends_on "berkeley-db"
  depends_on "libsndfile"
  depends_on "libsamplerate"
  depends_on "readline"

  conflicts_with "jack", :because => "jack2 is a replacement for jack"

  bottle do
    root_url "https://gitlab.zhdk.ch/TPF/homebrew-tpf-bottles/raw/master/bottles"
    sha256 "f1bc3ff41362f6d44e9b1f157dd105c604e5f9e9740cbbe8b4dc716bff847131" => :sierra
    sha256 "911579d2a20e44960346ff34a1d975c672450362e033fad70f40e5b602d08995" => :el_capitan
    sha256 "cc236dedd2a6dbea1e9f275c49afa7c18dab92c48756988e5a1f661c408033ae" => :yosemite
  end

  def install
    system "./waf", "configure",  "--prefix=#{prefix}"
    system "./waf", "build"
    system "./waf", "install"
  end

  plist_options :manual => "jackd -d coreaudio"

  def plist; <<-EOS.undent
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
      <key>Label</key>
      <string>#{plist_name}</string>
      <key>WorkingDirectory</key>
      <string>#{prefix}</string>
      <key>ProgramArguments</key>
      <array>
        <string>#{opt_bin}/jackd</string>
        <string>-d</string>
        <string>coreaudio</string>
      </array>
      <key>RunAtLoad</key>
      <true/>
      <key>KeepAlive</key>
      <true/>
    </dict>
    </plist>
    EOS
  end

  test do
    assert_match version.to_s, shell_output("#{bin}/jackd --version")
  end
end
