class QjackctlTpf < Formula
  desc "simple Qt application to control the JACK sound server daemon"
  homepage "https://qjackctl.sourceforge.io/"
  url "https://downloads.sourceforge.net/qjackctl/qjackctl-0.4.5.tar.gz"
  sha256 "c50da569ec8466ac6cc72c65e2d8212eb9c40149daed0a10fb7795ff9ddc4ab7"
  head "https://git.code.sf.net/p/qjackctl/code.git"

  bottle do
    root_url "https://gitlab.zhdk.ch/TPF/homebrew-tpf-bottles/raw/master/bottles"
    sha256 "5d4734eba2b5eab4c9a8dae1f462ecac75bd649960cfd1ca80f77076f362a042" => :sierra
    sha256 "990a1e199be99984f23853b97ec0fc181ee267ab6f078cec49297311c33085a2" => :el_capitan
    sha256 "a7353562fb0b590532897bef452bd00b62a38067c2c093fec701e646db480194" => :yosemite
  end

  depends_on "pkg-config" => :build
  depends_on "qt"
  depends_on "jack2"

  needs :cxx11

  def install
    ENV.cxx11
    system "./configure", "--disable-debug",
                          "--disable-dbus",
                          "--disable-portaudio",
                          "--disable-xunique",
                          "--prefix=#{prefix}",
                          "--with-jack=#{Formula["jack"].opt_prefix}",
                          "--with-qt5=#{Formula["qt"].opt_prefix}"

    system "make", "install"
    prefix.install bin/"qjackctl.app"
    bin.install_symlink prefix/"qjackctl.app/Contents/MacOS/qjackctl"
  end

  test do
    assert_match version.to_s, shell_output("#{bin}/qjackctl --version 2>&1", 1)
  end
end
