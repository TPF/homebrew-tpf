class Tpf < Formula
  desc "Tools for Telematic Performances Format"
  homepage "https://gitlab.zhdk.ch/tpf"
  version "0.1-test5"
  url "https://gitlab.zhdk.ch/TPF/telematic-performance-format/repository/archive.zip?ref=v0.1-test5"
  sha256 "3af13a59ddd70864e85300b1f2622c0698ae9bc1aab4b69a5fd75644107db7a0"

  depends_on "jacktrip"
  depends_on "qjackctl-tpf"
  depends_on "jack2"
  depends_on "jmess-tpf"
  depends_on "mtr"
  depends_on "iperf3"
  depends_on "pd-tpf"

  def install
    bin.install "tools/tpf-setup"
    bin.install "tools/tpf-netperf"
    pkgshare.install Dir["files/*"]
  end

  test do
    system "#{bin}/tpf-setup --do-nothing"
  end
end
